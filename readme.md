It's my solution to the coding challenge. Enjoy, **FTR**!

Łukasz Szkup

---

## Questions to requirements:

- The Halt command - should it pause the current interval (by remembering how much time left to another tick and continue from there later) or pause (stop) the timer and start on resume?
- Is user allowed to enter floating-point numbers or integers only?
- Should the timer start just after setting the interval or after entering at least one number?

By not having a business analyst at hand to clarify all of those, the KISS rule was applied for now.

## Answers to Part 2:

1. Only a separate view must be created (implementing IMainView) to align with the framework/technology of choice. The rest can stay as is.
2. Handling unusual input (like typos, extra spaces in commands, unknown commands), potentially adding more human readable error messages, handling exceptions in a way that the application does not crash or terminate, manual testing (besides automation).
3. The coding challenge does not involve big algorithmic or data structure knowledge but allows to check how a candidate would structure the code, deal with timers and do testing. Also, a few things in the requirements need clarification so there’s a good chance to check how a candidate would deal with that.
