import { getAnswer } from "./console-interface-async"

export const getConsole = (): IConsole => ({
  log: console.log,
  error: console.error,
  getAnswer,
})

export interface IConsole {
  getAnswer: (question: string) => Promise<string>
  log: (message: string) => void
  error: (message: string) => void
}
