import { createInterface } from "readline"

/**
 * Gets a user's answer after displaying a question.
 * It's a promise-based wrapper for the 'readline' library.
 * @param question The question
 * @returns The answer
 */
export const getAnswer = async (question: string): Promise<string> => {
  return new Promise<string>((resolve) => {
    const readline = createInterface({
      input: process.stdin,
      output: process.stdout,
    })
    readline.question(`${question}\n`, function (answer) {
      readline.close()
      resolve(answer)
    })
  })
}
