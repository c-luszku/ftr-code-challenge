import { MainController } from "./controllers/main-controller"
import { getFibonacciNumberTester } from "./domain/fibonacci-number-tester"
import { NumbersContainer } from "./domain/numbers-container"
import { getConsole } from "./helpers/console/console"
import { MainConsoleView } from "./views/main-console-view"

const fibonacciNumberTester = getFibonacciNumberTester(1000)
const numbersContainer = new NumbersContainer()
const console = getConsole()

const controller = new MainController(fibonacciNumberTester, numbersContainer)
const view = new MainConsoleView(controller, console)

view.run().catch(console.error)
