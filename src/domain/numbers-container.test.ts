import { NumbersContainer } from "./numbers-container"

describe("NumbersContainer: corner cases", () => {
  it("when no number is added to the container then an empty array is returned by getAllNumbers", () => {
    const container = new NumbersContainer()
    const allNumbers = container.getAllNumbers()
    expect(allNumbers.length).toBe(0)
  })
})

describe("NumbersContainer: regular case with numbers: 8, 5 and 8", () => {
  const container = new NumbersContainer()
  container.addNumber(8)
  container.addNumber(5)
  container.addNumber(8)
  const allNumbers = container.getAllNumbers()

  it("then getAllNumbers returns an array that has 2 items", () => {
    expect(allNumbers.length).toBe(2)
  })
  it("then getAllNumbers returns an array where the 1st item is 8 (higher frequency)", () => {
    expect(allNumbers[0].value).toBe(8)
  })
  it("then getAllNumbers returns an array where the 2nd item is 5 (lower frequency)", () => {
    expect(allNumbers[1].value).toBe(5)
  })
  it("then getAllNumbers returns an array where the frequency of the 1st item is 2", () => {
    expect(allNumbers[0].frequency).toBe(2)
  })
  it("then getAllNumbers returns an array where the frequency of the 2nd item is 1", () => {
    expect(allNumbers[1].frequency).toBe(1)
  })
})
