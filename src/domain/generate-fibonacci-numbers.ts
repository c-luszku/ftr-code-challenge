const firstTwoFibonacciNumbers = [0, 1]

/**
 * Generates Fibonacci numbers
 * @param length The number of Fibonacci numbers
 * @returns Fibonacci numbers
 */
export const generateFibonacciNumbers = (length: number): Array<number> => {
  if (length <= 0) return []
  if (length <= 2) return firstTwoFibonacciNumbers.slice(0, length)

  return new Array<number>(length - 2).fill(0).reduce((previousFibonacciNumbers) => {
    const nextNumber =
      previousFibonacciNumbers[previousFibonacciNumbers.length - 1] +
      previousFibonacciNumbers[previousFibonacciNumbers.length - 2]
    return [...previousFibonacciNumbers, nextNumber]
  }, firstTwoFibonacciNumbers)
}
