import { generateFibonacciNumbers } from "./generate-fibonacci-numbers"

const Maybe = "maybe"

export interface IFibonacciNumberTester {
  /**
   * Tests if a given number is a Fibonacci number
   * @param value Value to be tested
   * @returns A boolean if the result can be determined, otherwise "maybe" (it's when the number is bigger than the tester can handle)
   */
  isFibonacciNumber(value: number): boolean | typeof Maybe
}

interface IFibonacciNumberMap {
  map: Record<number, boolean>
  maximumFibonacciNumberValue: number
}

const getFibonacciNumberMap = (maximumTestableFibonacciNumber: number): IFibonacciNumberMap => {
  const fibonacciNumbers = generateFibonacciNumbers(maximumTestableFibonacciNumber)
  const maximumFibonacciNumberValue = fibonacciNumbers[fibonacciNumbers.length - 1]
  const fibonacciNumbersMap: Record<number, boolean> = fibonacciNumbers.reduce((map, number) => {
    return {
      ...map,
      [number]: true,
    }
  }, {})
  return {
    map: fibonacciNumbersMap,
    maximumFibonacciNumberValue,
  }
}

/**
 * Gets a Fibonacci number tester (IFibonacciNumberTester)
 * @param maximumTestableFibonacciNumber The maximum Fibonacci number that can be tested
 * @returns An instance implementing IFibonacciNumberTester
 */
export const getFibonacciNumberTester = (maximumTestableFibonacciNumber: number): IFibonacciNumberTester => {
  const { map: fibonacciNumbersMap, maximumFibonacciNumberValue } =
    getFibonacciNumberMap(maximumTestableFibonacciNumber)

  const isFibonacciNumber = (value: number) => {
    if (value > maximumFibonacciNumberValue) return Maybe
    return fibonacciNumbersMap[value] === true
  }

  return {
    isFibonacciNumber,
  }
}
