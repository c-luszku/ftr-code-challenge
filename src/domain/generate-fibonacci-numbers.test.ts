import { generateFibonacciNumbers } from "./generate-fibonacci-numbers"

describe("generateFibonacciNumbers: corner cases", () => {
  it("when the length is negative then an empty array is returned", () => {
    const numbers = generateFibonacciNumbers(-1)
    expect(numbers.length).toBe(0)
  })
  it("when the length is 0 then an empty array is returned", () => {
    const numbers = generateFibonacciNumbers(0)
    expect(numbers.length).toBe(0)
  })
  it("when the length is 1 then a 1-element array is returned", () => {
    const numbers = generateFibonacciNumbers(1)
    expect(numbers.length).toBe(1)
  })
  it("when the length is 2 then a 2-element array is returned", () => {
    const numbers = generateFibonacciNumbers(2)
    expect(numbers.length).toBe(2)
  })
})

describe("generateFibonacciNumbers: regular case", () => {
  const numbers = generateFibonacciNumbers(7)

  it("when the length is 7 then a 7-element array is returned", () => {
    expect(numbers.length).toBe(7)
  })
  it("when the length is 7 then the 1st item is the first Fibonacci number", () => {
    expect(numbers[0]).toBe(0)
  })
  it("when the length is 7 then the 2nd item is the second Fibonacci number", () => {
    expect(numbers[1]).toBe(1)
  })
  it("when the length is 7 then the 3rd item is the 3rd Fibonacci number", () => {
    expect(numbers[2]).toBe(1)
  })
  it("when the length is 7 then the 4th item is the 4th Fibonacci number", () => {
    expect(numbers[3]).toBe(2)
  })
  it("when the length is 7 then the 5th item is the 5th Fibonacci number", () => {
    expect(numbers[4]).toBe(3)
  })
  it("when the length is 7 then the 6th item is the 6th Fibonacci number", () => {
    expect(numbers[5]).toBe(5)
  })
  it("when the length is 7 then the 7th item is the 7th Fibonacci number", () => {
    expect(numbers[6]).toBe(8)
  })
})
