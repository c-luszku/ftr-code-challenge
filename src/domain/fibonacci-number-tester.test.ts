import { getFibonacciNumberTester } from "./fibonacci-number-tester"

interface ValueAndAnswer {
  value: number
  answer: boolean | "maybe"
}

describe("fibonacciNumberTester: corner cases", () => {
  const tester = getFibonacciNumberTester(7)
  const cornerCaseValuesAndExpectedAnswers: ValueAndAnswer[] = [
    { value: -1, answer: false },
    { value: 0, answer: true },
    { value: 1, answer: true },
    { value: 2, answer: true },
    { value: 7, answer: false },
    { value: 8, answer: true },
    { value: 13, answer: "maybe" },
    { value: 100, answer: "maybe" },
  ]

  cornerCaseValuesAndExpectedAnswers.forEach((testCase: ValueAndAnswer) => {
    it(`when the value ${testCase.value} is tested the following answer is expected: ${testCase.answer}`, () => {
      expect(tester.isFibonacciNumber(testCase.value)).toBe(testCase.answer)
    })
  })
})
