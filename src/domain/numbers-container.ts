export interface INumber {
  value: number
  frequency: number
}

export interface INumbersContainer {
  addNumber(value: number): void
  getAllNumbers(): INumber[]
}

export class NumbersContainer implements INumbersContainer {
  private numbersAndFrequency: Record<number, number | undefined> = {}

  addNumber(value: number): void {
    this.numbersAndFrequency[value] = (this.numbersAndFrequency[value] ?? 0) + 1
  }

  getAllNumbers(): INumber[] {
    return Object.keys(this.numbersAndFrequency)
      .map((k) => parseFloat(k))
      .map((value) => ({
        value,
        frequency: this.numbersAndFrequency[value] ?? 0,
      }))
      .sort((a: INumber, b: INumber) => b.frequency - a.frequency)
  }
}
