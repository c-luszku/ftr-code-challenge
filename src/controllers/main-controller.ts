import { IFibonacciNumberTester } from "../domain/fibonacci-number-tester"
import { INumber, INumbersContainer } from "../domain/numbers-container"
import { IMainView } from "../views/main-view"

export interface AddNumberResult {
  isFibonacciNumber: boolean | "maybe"
}

export class MainController {
  private outputtingIntervalInSeconds = 0
  private timer: NodeJS.Timer | undefined
  private view: IMainView | undefined

  constructor(private fibonacciNumberTester: IFibonacciNumberTester, private numbersContainer: INumbersContainer) {}

  setView(view: IMainView): void {
    this.view = view
  }

  setOutputtingInterval(intervalInSeconds: number): void {
    if (isNaN(intervalInSeconds)) throw new Error("intervalInSeconds must be a valid number")
    if (intervalInSeconds <= 0) throw new Error("intervalInSeconds must be positive")
    if (this.outputtingIntervalInSeconds) throw new Error("intervalInSeconds can be set only once")
    this.outputtingIntervalInSeconds = intervalInSeconds
    if (!this.timer) this.startTimer()
  }

  addNumber(value: number): AddNumberResult {
    this.numbersContainer.addNumber(value)
    return {
      isFibonacciNumber: this.fibonacciNumberTester.isFibonacciNumber(value),
    }
  }

  startTimer(): void {
    if (this.outputtingIntervalInSeconds <= 0) throw new Error("cannot start the timer as the interval is not set")
    if (this.timer) {
      this.stopTimer()
    }
    this.timer = setInterval(() => this.onTimerTicked(), this.outputtingIntervalInSeconds * 1000)
  }

  stopTimer(): void {
    if (this.timer) {
      clearInterval(this.timer)
      this.timer = undefined
    }
  }

  pauseTimer(): void {
    // TODO: a potential place for a more sophisticated solution after clarifying requirements
    this.stopTimer()
  }

  resumeTimer(): void {
    // TODO: a potential place for a more sophisticated solution after clarifying requirements
    this.startTimer()
  }

  getAllNumbers(): INumber[] {
    return this.numbersContainer.getAllNumbers()
  }

  private onTimerTicked() {
    this.view?.onTimerTicked()
  }
}
