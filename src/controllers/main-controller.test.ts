/* eslint-disable @typescript-eslint/no-empty-function */
import { IFibonacciNumberTester } from "../domain/fibonacci-number-tester"
import { INumbersContainer } from "../domain/numbers-container"
import { MainController } from "./main-controller"

afterEach(() => {
  jest.useRealTimers()
})

describe("MainController: when a negative value is passed to setOutputtingInterval", () => {
  const testCase = prepareTestCase()

  it("then an exception is thrown", () => {
    expect(() => testCase.controller.setOutputtingInterval(-1)).toThrow("intervalInSeconds must be positive")
  })
})

describe("MainController: when setOutputtingInterval is called the second time", () => {
  it("then an exception is thrown", () => {
    const testCase = prepareTestCase()
    testCase.controller.setOutputtingInterval(10)
    expect(() => testCase.controller.setOutputtingInterval(10)).toThrow("intervalInSeconds can be set only once")
  })
})

describe("MainController: when setOutputtingInterval is called with a valid number", () => {
  it("then a timer is started", () => {
    const testCase = prepareTestCase()
    const setIntervalSpy = jest.spyOn(global, "setInterval")
    testCase.controller.setOutputtingInterval(10)
    expect(setIntervalSpy).toHaveBeenLastCalledWith(expect.any(Function), 10000)
  })
})

describe("MainController: when an invalid value passed to setOutputtingInterval", () => {
  const testCase = prepareTestCase()

  it("then an exception is thrown", () => {
    expect(() => testCase.controller.setOutputtingInterval(NaN)).toThrow("intervalInSeconds must be a valid number")
  })
})

describe("MainController: when getAllNumbers is called", () => {
  const testCase = prepareTestCase()
  testCase.controller.getAllNumbers()

  it("then numbersContainer is used to get the data", () => {
    expect(testCase.mockGetAllNumbers).toHaveBeenCalled()
  })
})

describe("MainController: when addNumber is called for the first time", () => {
  const testCase = prepareTestCase()
  testCase.controller.setOutputtingInterval(5)
  testCase.controller.addNumber(12)

  it("then numbersContainer.addNumber is called", () => {
    expect(testCase.mockAddNumber).toHaveBeenCalledWith(12)
  })
})

describe("MainController: when a non-Fibonacci number gets added", () => {
  const testCase = prepareTestCase()
  testCase.controller.setOutputtingInterval(5)
  const addNumberResult = testCase.controller.addNumber(12)

  it("then isFibonacciNumber flag in the result of addNumber is set to false", () => {
    expect(addNumberResult.isFibonacciNumber).toBe(false)
  })
})

describe("MainController: when a Fibonacci number gets added", () => {
  const testCase = prepareTestCase(true)
  testCase.controller.setOutputtingInterval(5)
  const addNumberResult = testCase.controller.addNumber(12)

  it("then isFibonacciNumber flag in the result of addNumber is set to true", () => {
    expect(addNumberResult.isFibonacciNumber).toBe(true)
  })
})

describe("MainController: when startTimer is called", () => {
  const setIntervalSpy = jest.spyOn(global, "setInterval")
  const testCase = prepareTestCase(true)
  testCase.controller.setOutputtingInterval(5)
  testCase.controller.startTimer()

  it("then timer gets started", () => {
    expect(setIntervalSpy).toHaveBeenLastCalledWith(expect.any(Function), 5000)
  })
})

describe("MainController: when stopTimer is called", () => {
  const clearIntervalSpy = jest.spyOn(global, "clearInterval")
  const testCase = prepareTestCase(true)
  testCase.controller.setOutputtingInterval(5)
  testCase.controller.startTimer()
  testCase.controller.stopTimer()

  it("then timer gets stopped", () => {
    expect(clearIntervalSpy).toHaveBeenCalled()
  })
})

interface TestCase {
  controller: MainController
  mockIsFibonacciNumber: jest.Mock<boolean, []>
  mockAddNumber: jest.Mock<void, []>
  mockGetAllNumbers: jest.Mock<never[], []>
}

function prepareTestCase(isFibonacciNumberResult = false): TestCase {
  jest.useFakeTimers()
  const mockIsFibonacciNumber = jest.fn(() => isFibonacciNumberResult)
  const mockFibonacciTester: IFibonacciNumberTester = {
    isFibonacciNumber: mockIsFibonacciNumber,
  }

  const mockAddNumber = jest.fn(() => {})
  const mockGetAllNumbers = jest.fn(() => [])
  const mockNumbersContainer: INumbersContainer = {
    addNumber: mockAddNumber,
    getAllNumbers: mockGetAllNumbers,
  }
  const controller = new MainController(mockFibonacciTester, mockNumbersContainer)

  return {
    controller,
    mockIsFibonacciNumber,
    mockAddNumber,
    mockGetAllNumbers,
  }
}
