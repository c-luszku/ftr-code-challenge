/* eslint-disable @typescript-eslint/no-empty-function */
import { ImportMock } from "ts-mock-imports"
import * as mainControllerModule from "../controllers/main-controller"
import { AddNumberResult } from "../controllers/main-controller"
import { INumber } from "../domain/numbers-container"
import { IConsole } from "../helpers/console/console"
import { MainConsoleView } from "./main-console-view"

describe("MainConsoleView: when the user types `halt`", () => {
  let testCase: TestCase
  beforeAll(async () => {
    testCase = await prepareTestCase(["8", "10", "halt"])
  })
  afterAll(() => {
    ImportMock.restore()
  })

  it("then the timer gets paused", () => {
    expect(testCase.pauseTimerSpy).toHaveBeenCalledTimes(1)
  })
})

describe("MainConsoleView: when the timer is on and the user types `resume`", () => {
  let testCase: TestCase
  beforeAll(async () => {
    testCase = await prepareTestCase(["8", "10", "halt", "14", "resume"])
  })
  afterAll(() => {
    ImportMock.restore()
  })

  it("then the timer gets resumed", () => {
    expect(testCase.resumeTimerSpy).toHaveBeenCalledTimes(1)
  })
})

describe("MainConsoleView: when the timer types a Fibonacci number", () => {
  let testCase: TestCase
  beforeAll(async () => {
    testCase = await prepareTestCase(["8", "5"], true)
  })
  afterAll(() => {
    ImportMock.restore()
  })

  it("then the `FIB` message gets logged out", () => {
    expect(testCase.consoleLogSpy).toHaveBeenNthCalledWith(1, "FIB")
  })
})

describe("MainConsoleView: when the user sets interval to 8", () => {
  let testCase: TestCase
  beforeAll(async () => {
    testCase = await prepareTestCase(["8"])
  })
  afterAll(() => {
    ImportMock.restore()
  })

  it("at the beginning the user gets asked to enter the interval in secods", () => {
    expect(testCase.mockGetAnswer).toHaveBeenNthCalledWith(
      1,
      "Please input the number of time in seconds between emitting numbers and their frequency"
    )
  })

  it("then the outputting interval is set to 8 seconds", () => {
    expect(testCase.setOutputtingIntervalSpy).toHaveBeenCalledWith(8)
  })

  it("then the user gets asked to enter the first number", () => {
    expect(testCase.mockGetAnswer).toHaveBeenNthCalledWith(2, "Please enter the first number")
  })
})

describe("MainConsoleView: when the user sets interval to 8 and then passes numbers like: 5, 10, 5", () => {
  let testCase: TestCase
  beforeAll(async () => {
    testCase = await prepareTestCase(["8", "10", "5", "10"])
  })
  afterAll(() => {
    ImportMock.restore()
  })

  it("then the user gets asked to enter the first number", () => {
    expect(testCase.mockGetAnswer).toHaveBeenNthCalledWith(2, "Please enter the first number")
  })

  it("then the user gets asked to enter the next number", () => {
    expect(testCase.mockGetAnswer).toHaveBeenNthCalledWith(3, "Please enter the next number")
  })

  it("then the user gets asked to enter the next number again", () => {
    expect(testCase.mockGetAnswer).toHaveBeenNthCalledWith(4, "Please enter the next number")
  })
})

describe("MainConsoleView: when the user terminates the app", () => {
  let testCase: TestCase
  beforeAll(async () => {
    testCase = await prepareTestCase(["8", "10", "5", "10"])
  })
  afterAll(() => {
    ImportMock.restore()
  })

  it("then the numbers are retrieved from controller", () => {
    expect(testCase.getAllNumbersSpy).toHaveBeenNthCalledWith(1)
  })

  it("then the numbers are printed", () => {
    expect(testCase.consoleLogSpy).toHaveBeenNthCalledWith(1, "5:2, 10:1")
  })

  it("and the goodbye message gets printed", () => {
    expect(testCase.consoleLogSpy).toHaveBeenNthCalledWith(2, "Thanks for playing, press any key to exit.")
  })
})

interface TestCase {
  mockGetAnswer: jest.Mock<Promise<string>, []>
  setOutputtingIntervalSpy: jest.SpyInstance<void, [intervalInSeconds: number]>
  resumeTimerSpy: jest.SpyInstance<void, []>
  pauseTimerSpy: jest.SpyInstance<void, []>
  getAllNumbersSpy: jest.SpyInstance<INumber[], []>
  consoleLogSpy: jest.SpyInstance<void, [message: string]>
}

async function prepareTestCase(userAnswers: string[], isFibonacciNumberResult = false): Promise<TestCase> {
  const mockControllerManager = ImportMock.mockClass(mainControllerModule, "MainController")
  const controller = mockControllerManager.getMockInstance()

  // mock main controller
  const numbers: INumber[] = [
    { value: 5, frequency: 2 },
    { value: 10, frequency: 1 },
  ]
  mockControllerManager.mock("getAllNumbers", numbers)

  const addNumberResult: AddNumberResult = {
    isFibonacciNumber: isFibonacciNumberResult,
  }
  mockControllerManager.mock("addNumber", addNumberResult)

  // mock console
  let mockGetAnswer = jest.fn(() => Promise.resolve("quit")) // default (last action)
  userAnswers.forEach((q) => {
    mockGetAnswer = mockGetAnswer.mockImplementationOnce(() => Promise.resolve(q))
  })

  const mockConsole = ((): IConsole => {
    return {
      error: () => {},
      log: () => {},
      getAnswer: mockGetAnswer,
    }
  })()

  // set spies
  const setOutputtingIntervalSpy = jest.spyOn(controller, "setOutputtingInterval")
  const resumeTimerSpy = jest.spyOn(controller, "resumeTimer")
  const pauseTimerSpy = jest.spyOn(controller, "pauseTimer")
  const getAllNumbersSpy = jest.spyOn(controller, "getAllNumbers")
  const consoleLogSpy = jest.spyOn(mockConsole, "log")

  // Act
  const view = new MainConsoleView(controller, mockConsole)
  await view.run()

  return {
    mockGetAnswer,
    setOutputtingIntervalSpy,
    resumeTimerSpy,
    pauseTimerSpy,
    getAllNumbersSpy,
    consoleLogSpy,
  }
}
