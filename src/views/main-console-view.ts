import { MainController } from "../controllers/main-controller"
import { IConsole } from "../helpers/console/console"
import { IMainView } from "./main-view"

export class MainConsoleView implements IMainView {
  private isAtLeastOneNumberEntered = false

  constructor(private controller: MainController, private console: IConsole) {
    this.controller.setView(this)
  }

  async run(): Promise<void> {
    const interval = parseFloat(
      await this.console.getAnswer(
        "Please input the number of time in seconds between emitting numbers and their frequency"
      )
    )
    this.controller.setOutputtingInterval(interval)
    for (;;) {
      const answer = await this.console.getAnswer(
        this.isAtLeastOneNumberEntered ? "Please enter the next number" : "Please enter the first number"
      )
      if (this.inNumber(answer)) {
        const value = parseFloat(answer)
        this.handleNumber(value)
      } else if (this.processCommand(answer) === ProcessCommandResult.TerminationRequested) {
        this.console.log("Thanks for playing, press any key to exit.")
        break
      }
    }
  }

  onTimerTicked(): void {
    this.printNumbers()
  }

  private handleNumber(integer: number) {
    const result = this.controller.addNumber(integer)
    if (result.isFibonacciNumber === true) {
      this.console.log("FIB")
    }
    this.isAtLeastOneNumberEntered = true
  }

  private processCommand(command: string): ProcessCommandResult {
    switch (command) {
      case "halt":
        this.controller.pauseTimer()
        return ProcessCommandResult.CommandProcessed
      case "resume":
        this.controller.resumeTimer()
        return ProcessCommandResult.CommandProcessed
      case "quit":
        this.controller.stopTimer()
        this.printNumbers()
        return ProcessCommandResult.TerminationRequested
      default:
        return ProcessCommandResult.UnknownCommand
    }
  }

  private printNumbers(): void {
    const formattedNumbers = this.controller
      .getAllNumbers()
      .map((number) => `${number.value}:${number.frequency}`)
      .join(", ")
    if (formattedNumbers) this.console.log(formattedNumbers)
  }

  private inNumber(text: string): boolean {
    return !isNaN(parseFloat(text))
  }
}

enum ProcessCommandResult {
  CommandProcessed,
  TerminationRequested,
  UnknownCommand,
}
